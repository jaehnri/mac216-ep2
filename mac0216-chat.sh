#! /bin/bash
#  AO PREENCHER(MOS) ESSE CABEÇALHO COM O(S) MEU(NOSSOS) NOME(S) E
#  O(S) MEU(NOSSOS) NÚMERO(S) USP, DECLARO(AMOS) QUE SOU(MOS) O(S)
#  ÚNICO(S) AUTOR(ES) E RESPONSÁVEL(IS) POR ESSE PROGRAMA. TODAS AS
#  PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM DESENVOLVIDAS
#  E IMPLEMENTADAS POR MIM(NÓS) SEGUINDO AS INSTRUÇÕES DESSE EP E QUE
#  PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA OU PLÁGIO. DECLARO
#  TAMBÉM QUE SOU(MOS) RESPONSÁVEL(IS) POR TODAS AS CÓPIAS DESSE
#  PROGRAMA E QUE EU(NÓS) NÃO DISTRIBUÍ(MOS) OU FACILITEI(AMOS) A SUA
#  DISTRIBUIÇÃO. ESTOU(AMOS) CIENTE(S) QUE OS CASOS DE PLÁGIO E
#  DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
#  DIVULGADOS NA PÁGINA DA DISCIPLINA. ENTENDO(EMOS) QUE EPS SEM
#  ASSINATURA NÃO SERÃO CORRIGIDOS E, AINDA ASSIM, PODERÃO SER PUNIDOS
#  POR DESONESTIDADE ACADÊMICA.
#
#  Nome(s) : João Henri Carrenho Rocha
#  NUSP(s) : 11796378
#
#  Bibliography:
#       1) Create empty file: 
#           https://unix.stackexchange.com/questions/343548/how-do-i-create-a-new-empty-file-in-a-bash-script
#
#       2) Global function assignment: 
#           https://unix.stackexchange.com/questions/393928/global-local-variable-assignment
#
#       3) Passing parameters to bash function: 
#           https://stackoverflow.com/questions/6212219/passing-parameters-to-a-bash-function
#
#       4) Search exact string using grep: 
#           https://unix.stackexchange.com/questions/206903/match-exact-string-using-grep
#
#       5) Get second word in substring:
#           https://unix.stackexchange.com/questions/174037/extracting-the-second-word-from-a-string-variable/174038

########################## CONSTANTS ##########################

LOGGED_USERS_FILE=/tmp/logged_users
AUX_LOGGED_USERS_FILE=/tmp/aux
USERS_FILE=/tmp/users

SERVER=servidor
CLIENT=cliente

END_EXECUTION=end

################################################################################################################
#################################################### SERVER ####################################################
################################################################################################################

start_server() {
    > $LOGGED_USERS_FILE
    > $USERS_FILE
}

shutdown_server() {
    rm $LOGGED_USERS_FILE $USERS_FILE
}

list_active_users() {
    cut -d ' ' -f 1 < $LOGGED_USERS_FILE
}

set_start_time () {
    START_TIME=$(date +%s)
}

get_uptime() {
    echo $(($(date +%s) - $START_TIME))
}

reset() {
   shutdown_server
   start_server 
}

close_server() {
    shutdown_server
    end_program
}

################################################################################################################
#################################################### CLIENT ####################################################
################################################################################################################

create_user() {
	if grep -q "^$1 " $USERS_FILE; then
        echo ERRO
		return 1
	fi

	echo $@ >> $USERS_FILE
}

user_has_active_session() {
    if grep -q "^$1 " $LOGGED_USERS_FILE; 
    then
        return 0
    fi

    return 1
}

terminal_has_active_session() {
    if grep -q "$(tty)\$" $LOGGED_USERS_FILE;
    then
        return 0
    fi

    return 1
}

credentials_exist () {
    grep -q -w "^$1 $2" $USERS_FILE
}

change_password() {
    if credentials_exist $1 $2;
    then
        sed -i "s/^$1 $2\$/$1 $3/" $USERS_FILE
        return 0
    fi

    echo ERRO
    return 1
}

log_user_in() {
    if user_has_active_session $1 || terminal_has_active_session || ! credentials_exist $1 $2;
    then
        echo ERRO
		return 1
	fi

    echo $1 $(tty) >> $LOGGED_USERS_FILE
}

log_user_out() {
    if terminal_has_active_session;
    then
        grep -v "$(tty)\$" $LOGGED_USERS_FILE > $AUX_LOGGED_USERS_FILE; mv $AUX_LOGGED_USERS_FILE $LOGGED_USERS_FILE
        return 0
    fi

    echo ERRO
    return 1
}

list_users_client() {
    if terminal_has_active_session;
    then
        list_active_users
        return 0
    fi

    echo ERRO
    return 1
}

send_message() {
    if terminal_has_active_session || user_has_active_session $1;
    then
        SENDER=$(grep "$(tty)" $LOGGED_USERS_FILE | awk '{print $1}')
	    RECEIVER_TERMINAL=$(grep "^$1 " $LOGGED_USERS_FILE | awk '{print $2}')
        MESSAGE=$(echo $INPUT | sed "s/msg $1 /[Mensagem de $SENDER]: /")

        echo > $RECEIVER_TERMINAL
        echo $MESSAGE > $RECEIVER_TERMINAL
        echo -n "cliente> " > $RECEIVER_TERMINAL


        return 0
    fi

    echo ERRO
    return 1
}

quit_client() {
    log_user_out
    end_program
}

################################################################################################################
#################################################### UTIL ######################################################
################################################################################################################

end_program() {
    EXECUTION_MODE=$END_EXECUTION
}

get_execution_mode() {
    case $1 in
        $CLIENT) EXECUTION_MODE=$1;;
        $SERVER) EXECUTION_MODE=$1
            set_start_time
            start_server;;
        *) printf "The only parameter should be the execution mode.\nTry ./mac0216-chat.sh servidor or ./mac0216-chat.sh cliente \n"
        exit 1;;
    esac
}

server_loop() {
    while [ $EXECUTION_MODE != $END_EXECUTION ]; do
	echo -n $EXECUTION_MODE\>\ 
	INPUT=""
	read INPUT

	case ${INPUT%% *} in
		time) get_uptime;;
        list) list_active_users;;
        reset) reset;;
        quit) close_server;;
		*) echo ERRO;;
	esac
done
}

client_loop() {
    while [ $EXECUTION_MODE != $END_EXECUTION ]; do
	echo -n $EXECUTION_MODE\>\ 
	INPUT=""
	read INPUT

	case ${INPUT%% *} in
        create) create_user ${INPUT#* };;
        login) log_user_in ${INPUT#* };;
        logout) log_user_out ${INPUT#* };;
        passwd) change_password ${INPUT#* };;
        list) list_users_client;;
        msg) send_message ${INPUT#* };;
        quit) quit_client;;
		*) echo ERRO;;
	esac
done
}

################################################################################################################
#################################################### MAIN THREAD ###############################################
################################################################################################################

get_execution_mode $1

if [ $EXECUTION_MODE == $SERVER ]
then
    server_loop
else
    client_loop
fi
